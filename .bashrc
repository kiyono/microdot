# ~/.bashrc: executed by bash(1) for non-login shells.
#
# Description: Microdot - The Electric Dotfiles Acid Test
# Author: Craig Kiyono <code@kiyono.ca>
# Date: 2023-04-01
# Bash Version: 5.1.16
# License: MIT License
# License URL: https://opensource.org/licenses/MIT
# Version: 0.1.0

# If not running interactively, don't do anything.
[[ -z ${PS1} ]] && return

# Load all bash configuration files from .bashrc.d directory.
if [[ -d ~/.bashrc.d ]]; then
    for bash_config in ~/.bashrc.d/.bash*; do
        source "${bash_config}"
    done
fi

if [[ -f /usr/share/bash-completion/bash_completion ]]; then
    source /usr/share/bash-completion/bash_completion
elif [[ -f /etc/bash_completion ]]; then
    source /etc/bash_completion
fi

if [[ -f ~/.bashrc.d/.dircolors ]]; then
    eval "$(dircolors ~/.bashrc.d/.dircolors)"
else
    eval "$(dircolors)"
fi

shopt -s checkwinsize  # Update window size after each command.
shopt -s cmdhist       # Save all lines of a multiline command to the same history entry.
shopt -s histappend    # Append to history file when shell exits, rather than overwriting.
shopt -s lithist       # Save multiline commands with embedded newlines.

export HISTCONTROL=ignoreboth   # History ignores duplicates lines and lines starting with a space.
export HISTFILESIZE=2000        # The maximum number of lines contained in the history file.
export HISTSIZE=1000            # The maximum number of commands to remember on the history list.
export HISTTIMEFORMAT='%F %T '  # Use standard ISO 8601 timestamp %Y-%m-%d %H:%M:%S (24-hours format).
export MANPAGER='less -X'       # Don't clear the screen after quitting a manual page.
export PROMPT_DIRTRIM=6         # Number of trailing directory components to retain when expanding \w and \W.

bind 'set completion-ignore-case on'  # Perform file completion in a case insensitive fashion.
bind 'set mark-directories on'        # Add a trailing slash whesn completing a directory name.
bind 'set show-all-if-ambiguous on'   # Display all possible matches when there is more than one possible completion.
bind 'set skip-completed-text on'     # Skips any portion of the command line that has already been completed.

if [[ -x "$(command -v pip)" ]]; then
    export PIP_REQUIRE_VIRTUALENV=true  # Ensures that python packages are installed in an isolated environment.
fi

if [[ -f /usr/lib/git-core/git-sh-prompt ]]; then
    source /usr/lib/git-core/git-sh-prompt
    export GIT_PS1_SHOWUPSTREAM="verbose name"
fi

function __set_prompt() {

    local exit_status="$?"  # Captures the exit status from the previous command.

    local reset="\[\e[0m\]"
    local red="\[\e[38;2;204;0;0m\]"             #cc0000
    local yellow="\[\e[38;2;252;233;79m\]"       #fce94f
    local dark_yellow="\[\e[38;2;237;212;0m\]"   #edd400
    local orange="\[\e[38;2;252;175;62m\]"       #fcaf3e
    local dark_orange="\[\e[38;2;245;121;0m\]"   #f57900
    local green="\[\e[38;2;138;226;52m\]"        #8ae234
    local cyan="\[\e[38;2;52;226;226m\]"         #34e2e2
    local blue="\[\e[38;2;114;159;207m\]"        #729fcf
    local purple="\[\e[38;2;173;127;168m\]"      #ad7fa8
    local dark_purple="\[\e[38;2;117;80;123m\]"  #75507b
    local white="\[\e[38;2;238;238;236m\]"       #eeeeec
    local grey="\[\e[38;2;186;189;182m\]"        #babdb6
    local charcoal="\[\e[38;2;65;65;65m\]"       #414141

    # Date and timestamp
    PS1="\n${charcoal}$(date "+%Y-%m-%d") \t "

    # Python virtual environment
    if [[ -n ${VIRTUAL_ENV} ]]; then
        PS1+="${cyan}($(basename ${VIRTUAL_ENV})) "
    fi

    # Username
    local user_colour="${grey}"
    if [[ "${USER}" == "root" ]]; then
        user_colour="${red}"
    fi

    PS1+="${user_colour}\u"

    # Hostname
    local hostname_colour="${grey}"
    if [[ -n ${SSH_CONNECTION} ]]; then
        hostname_colour="${green}"
    fi

    PS1+="${charcoal}@${hostname_colour}$(hostname -f) "

    # Current working directory
    PS1+="${white}\w${reset}"

    # Git branch
    if [[ -x "$(command -v git)" ]] && type __git_ps1 &>/dev/null; then

        local git_status="$(git status 2> /dev/null)"

        # Set Git status colour based on state of working directory.
        local git_ps1_colour="${green}"
        if [[ ${git_status} =~ "Changes to be committed" ]]; then
            git_ps1_colour="${yellow}"
        elif [[ ${git_status} =~ "untracked files present" || ${git_status} =~ "no changes added to commit" ]]; then
            git_ps1_colour="${red}"
        fi

        PS1+="${git_ps1_colour}$(__git_ps1)${reset}"
    fi

    # Previous command exit status
    if [[ ${exit_status} -ne 0 ]]; then
        PS1+=" ${red}${exit_status}${reset}"
    fi

    # Job status
    local num_background_jobs=$(jobs -r | wc -l)
    local num_suspended_jobs=$(jobs -s | wc -l)

    local job_status_colour="${white}"
    if [[ ${num_background_jobs} -gt 2 || ${num_suspended_jobs} -gt 2 ]]; then
        job_status_colour="${red}"
    elif [[ ${num_background_jobs} -gt 0 || ${num_suspended_jobs} -gt 0 ]]; then
        job_status_colour="${orange}"
    fi

    PS1+="\n${job_status_colour}\$ ${reset}"

    # Window title
    PS1+="\[\e]0;\u@$(hostname -f) \w\a\]"
}

export PROMPT_COMMAND=__set_prompt
