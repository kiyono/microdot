# Directory navigation.
alias ..='cd ..; ls'
alias ...='cd ../../; ls'
alias ....='cd ../../../; ls'
alias .....='cd ../../../../; ls'

# Display disk space available and show file system type.
alias df='df --human-readable --si --print-type --exclude-type=squashfs'
alias ds='df --human-readable --si --print-type --exclude-type=squashfs --exclude-type=tmpfs --exclude-type=devtmpfs'

alias du='du --human-readable'

# Display disk space usage for the current directory or a specified directory.
alias ducurrent='du --summarize --human-readable'

# Display disk space usage of directories one level deep.
alias dulocal='du --block-size=1K --human-readable --max-depth=1'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

alias ls='ls --almost-all --color=auto --dereference-command-line --format=long --group-directories-first --human-readable --quoting-style=literal --time-style=long-iso -v'

alias lr='ls -AHlhR --color=auto --group-directories-first --quoting-style=literal --time-style=long-iso'  # List subdirectories recursively.
alias lx='ls -AHlhX --color=auto --group-directories-first --quoting-style=literal --time-style=long-iso'  # Sort by extension.
alias lk='ls -AHlhS --color=auto --group-directories-first --quoting-style=literal --time-style=long-iso'  # Sort by size.
alias lt='ls -AHlht --color=auto --group-directories-first --quoting-style=literal --time-style=long-iso'  # Sort by date.
alias lc='ls -AHlhc --color=auto --group-directories-first --quoting-style=literal --time-style=long-iso'  # Sort by modification time.
alias lu='ls -AHlhu --color=auto --group-directories-first --quoting-style=literal --time-style=long-iso'  # Sort by access time.

alias cp='cp --verbose'
alias mkdir='mkdir --verbose'
alias mv='mv --verbose'
alias rm='rm --interactive=once --verbose'

# Fail to operate recursively on '/'.
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

# Source .bashrc.
alias src='source ${HOME}/.bashrc'

alias reboot='sudo shutdown --reboot now'
alias shutdown='sudo shutdown --poweroff now'

# Delete history and clear terminal.
alias thoughtcrime='history -c && history -w && clear'

# Recursively chmod directories to default.
alias chmoddir='find . -type d -exec chmod 755 {} +'

# Recursively chmod files to default.
alias chmodfiles='find . -type f -exec chmod 644 {} +'

# Display local IP addresses.
alias iplocal='ip -brief -color=auto a'

# Display open sockets.
alias lsock='sudo lsof -iP'

# Display open TCP sockets.
alias lsocktcp='sudo lsof -nP | grep TCP'

# Display open UDP sockets.
alias lsockudp='sudo lsof -nP | grep UDP'

# Print each $PATH entry on a separate line.
alias path='echo -e ${PATH//:/\\n}'

# Microdot
alias microdot='git --git-dir=${HOME}/.microdot/ --work-tree=${HOME}'
alias microdotupdate='git --git-dir=${HOME}/.microdot/ --work-tree=${HOME} pull'

# Git
alias gadd='git add . && git status'
alias gamend='git commit --amend --no-edit'
alias gcom='git commit -m'
alias gmain='git checkout main'
alias gstat='git status'

# SSH
alias sshpubkey='cat ${HOME}/.ssh/id_ed25519.pub'

# List all loaded services.
alias servicesall='systemctl list-units --type=service'

# List all running services.
alias servicesrunning='systemctl list-units --type=service --state=running'

# List all failed services.
alias servicesfailed='systemctl list-units --type=service --state=failed'

# Get the status of a service.
alias servicestatus='sudo systemctl status'

# Start or stop services.
alias servicestart='sudo systemctl start'
alias servicestop='sudo systemctl stop'
alias servicekill='sudo systemctl kill'
alias servicerestart='sudo systemctl restart'
alias servicereload='sudo systemctl reload'

# cal
if [[ -x "$(command -v cal)" ]]; then
    alias jan='cal -m 01'
    alias feb='cal -m 02'
    alias mar='cal -m 03'
    alias apr='cal -m 04'
    alias may='cal -m 05'
    alias jun='cal -m 06'
    alias jul='cal -m 07'
    alias aug='cal -m 08'
    alias sep='cal -m 09'
    alias oct='cal -m 10'
    alias nov='cal -m 11'
    alias dec='cal -m 12'
fi

# Uncomplicated Firewall
if [[ -x "$(command -v ufw)" ]]; then
    alias ufwenable='sudo ufw enable'
    alias ufwdisable='sudo ufw disable'
    alias ufwlog='sudo tail -f /var/log/ufw.log'
    alias ufwstatus='sudo ufw status verbose'
    alias ufwstatusnum='sudo ufw status numbered'
fi

# Debian/Ubuntu
if [[ -f /etc/debian_version ]]; then
    alias has='apt show'
    alias update='sudo apt update && apt list --upgradable'
    alias upgrade='sudo apt upgrade'
    alias clean='sudo apt autoremove && sudo apt clean'
    alias pkginstall='sudo apt install'
    alias pkglist='sudo apt list --installed'
    alias pkgremove='sudo apt remove'
    alias pkgsearch='sudo apt search'


    if [[ -x "$(command -v snap)" ]]; then
        alias snaphas='snap info'
        alias snapupgrade='sudo snap refresh'
        alias snapclean='LANG=C snap list --all | while read snapname ver rev trk pub notes; do if [[ $notes = *disabled* ]]; then sudo snap remove "$snapname" --revision="$rev"; fi; done'
        alias snapinstall='sudo snap install'
        alias snaplist='snap list'
        alias snaplistupdate='sudo snap refresh --list'
        alias snapremove='sudo snap remove'
        alias snapsearch='snap find'
        alias snapsize='du -hcs /var/lib/snapd/snaps/*'
    fi

# Fedora
elif [[ -f /etc/fedora-release ]]; then
    alias has='dnf info'
    alias update='sudo dnf check-update'
    alias upgrade='sudo dnf upgrade'
    alias clean='sudo dnf autoremove'
    alias pkginstall='sudo dnf install'
    alias pkglist='dnf list installed'
    alias pkgremove='sudo dnf remove'
    alias pkgsearch='sudo dnf search'
    alias pkgdependencies='yum whatprovides'

# CentOS/RHEL
elif [[ -f /etc/redhat-release ]]; then
    alias has='yum info'
    alias update='sudo yum check-update'
    alias upgrade='sudo yum update'
    alias clean='sudo yum clean all'
    alias pkginstall='sudo yum install'
    alias pkglist='yum list installed'
    alias pkgremove='sudo yum remove'
    alias pkgsearch='sudo yum search'
    alias pkgdependencies='yum whatprovides'
fi
